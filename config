#!/bin/bash

#/////////////////////////////////////////
#////////////	Variabels Set	//////////
#/////////////////////////////////////////

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
font xft:Ubuntu Regular 14

# Use Mouse+$mod to drag floating windows
floating_modifier $mod

focus_follows_mouse no







#/////////////////////////////////////////
#////////////	color def     	//////////
#/////////////////////////////////////////

# MONOKAI

set $color0     #101010
set $color1     #960050
set $color2     #66aa11
set $color3     #c47f2c
set $color4     #30309b
set $color5     #7e40a5
set $color6     #3579a8
set $color7     #9999aa

set $color8     #303030
set $color9     #ff0090
set $color10    #80ff00
set $color11    #ffba68
set $color12    #5f5fee
set $color13    #bb88dd
set $color14    #4eb4fa
set $color15    #d0d0d0



# class                     border      backgr.     text        indicator       child_border
# ----------------------------------------------------------------------------------------
client.focused              $color3     $color0     $color15     $color9         $color2
client.focused_inactive     $color3     $color0     $color15     $color9         $color7

client.unfocused            $color3     $color0     $color15     $color9         $color8
client.urgent               $color3     $color0     $color15     $color9         $color1

client.placeholder          $color8     $color0     $color15     $color9         $color6

client.background           $color0







#/////////////////////////////////////////
#////////////	Apps Startup	//////////
#/////////////////////////////////////////

# Autostart tray applications
exec --no-startup-id nm-applet
exec --no-startup-id volumeicon
exec --no-startup-id pamac-tray

# Autostart Apps
exec --no-startup-id /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
exec --no-startup-id xfce4-power-manager
exec --no-startup-id xautolock -time 30 -locker 'i3lock -d -c 000000'
exec --no-startup-id dunst -conf ~/.config/i3/dunst/dunstrc
exec --no-startup-id picom --config ~/.config/i3/picom/picom.conf -b
exec --no-startup-id nitrogen --restore
exec --no-startup-id conky -c ~/.config/i3/conky/conky_new.lua
exec xrdb ~/.Xdefaults







#/////////////////////////////////////////
#////////////	Key Managment	//////////
#/////////////////////////////////////////

# Set mod key (Mod1=<Alt>, Mod4=<Super>)
set $mod Mod4

# split orientation
bindsym $mod+q                  split toggle
bindsym $mod+h                  split h; exec notify-send 'tile horizontally'
bindsym $mod+v                  split v; exec notify-send 'tile vertically'

# App Start
bindsym $mod+Return             exec alacritty;focus
bindsym $mod+Shift+Return       exec firefox
bindsym $mod+Ctrl+Return        exec nemo
bindsym $mod+Mod1+Return        exec rofi -show drun

bindsym $mod+F1                 exec python ~/.config/i3/help.py
bindsym $mod+F2                 exec rofi -show combi
bindsym $mod+F3                 exec code
bindsym $mod+F4                 exec com.axosoft.GitKraken

# bindsym $mod+F5               exec
# bindsym $mod+F6               exec
bindsym $mod+F7                 exec virtualbox
bindsym $mod+F8                 exec steam-runtime

# kill focused window
bindsym $mod+Shift+q            kill

# Picom keys
bindsym $mod+t                  exec --no-startup-id picom --config ~/.config/i3/picom/picom.conf -b
bindsym $mod+Ctrl+t             exec --no-startup-id pkill picom



# alternatively, you can use the cursor keys:
bindsym $mod+Left               focus left
bindsym $mod+Down               focus down
bindsym $mod+Up                 focus up
bindsym $mod+Right              focus right

# move tiles:
bindsym $mod+Shift+Left         move left
bindsym $mod+Shift+Down         move down
bindsym $mod+Shift+Up           move up
bindsym $mod+Shift+Right        move right

#navigate workspaces next / previous
bindsym $mod+Ctrl+Right         workspace next
bindsym $mod+Ctrl+Left          workspace prev

# toggle fullscreen mode for the focused container
bindsym $mod+f                  fullscreen toggle

# toggle tiling / floating
bindsym $mod+Shift+space        floating toggle

# change focus between tiling / floating windows
bindsym $mod+space              focus mode_toggle

# reload the configuration file
bindsym $mod+Shift+c            reload

# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r            restart

# reset dunst
bindsym $mod+Shift+d --release exec "killall dunst; exec notify-send 'restart dunst'"







#/////////////////////////////////////////////////
#////////////	Workspace Managment	    //////////
#/////////////////////////////////////////////////

set $ws1 "1: Desktop"
set $ws2 "2: Desktop"
set $ws3 "3: Desktop"
set $ws4 "4: Desktop"
set $ws5 "5: Desktop"
set $ws6 "6: Desktop"
set $ws7 "7: Desktop"
set $ws8 "8: Desktop"

# switch to workspace
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8

# Move focused container to workspace
bindsym $mod+Ctrl+1 move container to workspace $ws1
bindsym $mod+Ctrl+2 move container to workspace $ws2
bindsym $mod+Ctrl+3 move container to workspace $ws3
bindsym $mod+Ctrl+4 move container to workspace $ws4
bindsym $mod+Ctrl+5 move container to workspace $ws5
bindsym $mod+Ctrl+6 move container to workspace $ws6
bindsym $mod+Ctrl+7 move container to workspace $ws7
bindsym $mod+Ctrl+8 move container to workspace $ws8

# Move to workspace with focused container
bindsym $mod+Shift+1 move container to workspace $ws1; workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2; workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3; workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4; workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5; workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6; workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7; workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8; workspace $ws8

# Open applications on specific workspaces
# assign [class="Chromium"] $ws1
# assign [class=""] $ws2
# assign [class=""] $ws3
# assign [class=""] $ws4
# assign [class=""] $ws5
# assign [class=""] $ws6
# assign [class="VirtualBox"] $ws7
# assign [class="Steam"] $ws8







#/////////////////////////////////////////////////
#////////////	Window Managment	    //////////
#/////////////////////////////////////////////////

new_window  pixel 5
new_float   pixel 5

gaps inner 30

set $mode_gaps Gaps: (o) outer, (i) inner
set $mode_gaps_outer Outer Gaps: +|-|0 (local), Shift + +|-|0 (global)
set $mode_gaps_inner Inner Gaps: +|-|0 (local), Shift + +|-|0 (global)
bindsym $mod+Shift+g mode "$mode_gaps"

mode "$mode_gaps" {
    bindsym o      mode "$mode_gaps_outer"
    bindsym i      mode "$mode_gaps_inner"
    bindsym Return mode "default"
    bindsym Escape mode "default"
}

mode "$mode_gaps_inner" {
    bindsym plus  gaps inner current plus 5
    bindsym minus gaps inner current minus 5
    bindsym 0     gaps inner current set 0

    bindsym Shift+plus  gaps inner all plus 5
    bindsym Shift+minus gaps inner all minus 5
    bindsym Shift+0     gaps inner all set 0

    bindsym Return mode "default"
    bindsym Escape mode "default"
}

mode "$mode_gaps_outer" {
    bindsym plus  gaps outer current plus 5
    bindsym minus gaps outer current minus 5
    bindsym 0     gaps outer current set 0

    bindsym Shift+plus  gaps outer all plus 5
    bindsym Shift+minus gaps outer all minus 5
    bindsym Shift+0     gaps outer all set 0

    bindsym Return mode "default"
    bindsym Escape mode "default"
}

# Resize window (you can also use the mouse for that)
bindsym $mod+r mode "resize"

mode "resize" {
    # These bindings trigger as soon as you enter the resize mode
    # same bindings, but for the arrow keys

    bindsym Left resize shrink width 10 px or 10 ppt
    bindsym Down resize grow height 10 px or 10 ppt
    bindsym Up resize shrink height 10 px or 10 ppt
    bindsym Right resize grow width 10 px or 10 ppt


    # exit resize mode: Enter or Escape

    bindsym Return mode "default"
    bindsym Escape mode "default"
}

# Open specific applications in floating mode
for_window [title="alsamixer"] floating enable
for_window [class="Calamares"] floating enable
for_window [class="Clipgrab"] floating enable
for_window [title="File Transfer*"] floating enable
for_window [class="GParted"] floating enable
for_window [class="Lxappearance"] floating enable
for_window [class="Nitrogen"] floating enable
for_window [class="Oblogout"] fullscreen enable
for_window [class="Pamac-updater"] floating enable
for_window [class="Pavucontrol"] floating enable
for_window [class="Skype"] floating enable
for_window [class="Thus"] floating enable
for_window [class="Timeset-gui"] floating enable
for_window [class="Xfce4-appfinder"] floating enable
for_window [class="conky_help"] floating enable
# for_window [class="Pamac-manager"] floating enable
# for_window [class="(?i)virtualbox"] floating enable

# switch to workspace with urgent window automatically
for_window [urgent=latest] focus







#/////////////////////////////////////////////////
#////////////	Statusbar Managment	     /////////
#/////////////////////////////////////////////////

bar {

    status_command i3blocks -c ~/.config/i3/i3blocks/config
	# status_command i3status -c ~/.config/i3/i3status.conf

	position top
	font xft:square 10
    # separator_symbol ":|:"
 	tray_padding 0
	strip_workspace_numbers no
    strip_workspace_name yes
    workspace_buttons yes

    binding_mode_indicator yes

  	colors {

        background    $color0
	    statusline    $color7 # bar text
		separator     $color15

		#     	              Rahmen 	   Hintergrund	   Text
		#---------------------------------------------------------
		focused_workspace     $color10 	   $color2 	       $color0

        active_workspace      $color7 	   $color8 	       $color0
		inactive_workspace    $color8 	   $color8 	       $color0

        urgent_workspace      $color1 	   $color9 	       $color0

        binding_mode          #2f343a 	   #900000 	       #ffffff
	}
}







#////////////////////////////////////////////
#///////////    	   EXIT    	   //////////
#////////////////////////////////////////////

bindsym $mod+0 mode "$mode_system"

set $mode_system (L)ock,       (E)xit,         (R)eboot,       (S)hutdown

mode "$mode_system" {
    bindsym l exec i3lock -i ~/Bilder/7.png, mode "default"
    bindsym e exec i3-msg exit, mode "default"
    bindsym r exec reboot, mode "default"
    bindsym s exec poweroff, mode "default"

    bindsym Return mode "default"
    bindsym Escape mode "default"
}
