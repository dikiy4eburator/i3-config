#!/usr/bin/python3

# PrimeArch  Copyright (C) 2020  Wladimir Frank

from subprocess import call
from time import sleep, time

from e_ascii import (
    grafik_engine,
    draw_text,
    draw_text_middle,
    draw_text_right,
    draw_line,
    draw_line_middle,
)

text = [

    [2,  "──────────────────────────────────────────────────────────────────────"],
    [46, "──────────────────────────────────────────────────────────────────────"],
    [5,  "────────────────────────────────────────────────────────────"],
    [10, "────────────────────────────────────────────────────────────"],
    [23, "────────────────────────────────────────────────────────────"],
    [33, "────────────────────────────────────────────────────────────"],
    [41, "────────────────────────────────────────────────────────────"],
    
    [4,  "Desktop control"],
    [9,  "Stack control"],
    [22, "Window control"],
    [32, "Main apps"],
    [40, "System funktions"],
    
    [6,  "Mod + 1-8",                    "Switch to desktop Nr"],
    [11, "Mod + q",                     "toggle split"],
    [13, "Mod + Alt + k",               "Add Stack"],
    [14, "Mod + Alt + j",               "Delete Stack"],
    [15, "Mod + alt + h",               "Resize stack left"],
    [16, "Mod + alt + l",               "Resize stack right"],
    [18, "Mod + Up/Down",               "List through Stack"],
    [19, "Mod + Right/Left",            "Switch Stack focus"],
    [24, "Mod + Shift + q",             "kill window"],
    [25, "Mod + f",                     "toggle floating"],
    [27, "Mod + Shift + Up/Down",       "Move Window in Stack Up/Down"],
    [28, "Mod + Shift + Right/Left",    "Move Window to Stack"],
    [29, "Mod + Shift + 1-8",           "Move window to desktop Nr"],
    [34, "Mod + Enter",                 "Terminal"],
    [35, "Mod + Shift + Enter",         "Browser"],
    [36, "Mod + Ctrl + Enter",          "File Manager"],
    [37, "Mod + Alt + Enter",           "Rofi-drun"],
    [42, "Mod + r",                     "reload Qtile"],
    [43, "Mod + 0",                     "Exit Qtile"]
]


def screan_help(engine):

    screan_mid = int(engine.canvas_x[1] / 2)

    orange = engine.color_conv.rgb_to_color("f", 0, 196, 127, 44)
    green = engine.color_conv.rgb_to_color("f", 0, 102, 170, 17)
    red = engine.color_conv.rgb_to_color("f", 0, 150, 0, 80)

    engine.draw_list.clear()
    engine.clear_frame_buffer()

    # draw lines
    for element in text[:7]:
        engine.add_drawer(
            draw_line_middle(
                engine,
                start_vektor=[screan_mid, element[0] - 1],
                lengh=len(element[1]),
                orientation="h",
                char="─"
            )
        )
    
    # draw block title
    for element in text[7:12]:
        engine.add_drawer(
            draw_text_middle(
                engine,
                start_vektor=[screan_mid, element[0] - 1],
                text=element[1],
                color_slot_1=green
            )
        )
    
    # draw help text
    for element in text[12:30]:
        engine.add_drawer(
            draw_text_right(
                engine,
                start_vektor=[screan_mid - 3, element[0] - 1],
                text=element[1],
                color_slot_1=red
            )
        )

        engine.add_drawer(
            draw_text_middle(
                engine,
                start_vektor=[screan_mid, element[0] - 1],
                text="<->",
                color_slot_1=orange
            )
        )

        engine.add_drawer(
            draw_text(
                engine,
                start_vektor=[screan_mid + 4, element[0] - 1],
                text=element[2]
            )
        )
    
    # deco
    engine.add_drawer(
            draw_text_middle(
                engine,
                start_vektor=[screan_mid, 46],
                text="[ESC] EXIT"
            )
        )

    
if __name__ == "__main__":

    engine = grafik_engine()

    # HIDE PROMPT
    call(["tput", "civis"])

    main_loop_run = True
    FPS = 25

    # draw logout screan
    screan_help(engine)

    while main_loop_run:
        frame_start = time()

        # DRAW FRAME BUFFER
        if not engine.pause:
            engine.draw_frame_buffer()

        # INPUT BLOCK
        if not engine.key_buffer.empty():
            input_main_loop = engine.key_buffer.get()

            if input_main_loop == "KEY_ESCAPE":
                main_loop_run = False

        # FPS KONTROLER
        frame_time = time() - frame_start
        if frame_time < 1 / FPS:
            sleep(1 / FPS - frame_time)

    # SHOW PROMPT AND CLEAN THE SHELL
    call(["tput", "cnorm"])
    call(["clear"])
